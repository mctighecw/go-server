#!/bin/bash

echo Deleting database...

DB_USER="go_user"
DB_NAME="go_foods"

echo Closing any open db connections...
sudo -u $USER psql postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '$DB_NAME' AND pid <> pg_backend_pid();"

echo Deleting old db...
sudo -u $USER dropdb $DB_NAME

# echo Deleting old user...
# sudo -u $USER dropuser $DB_USER
