INSERT INTO foods (name, cuisine, healthy, popularity, rating)
VALUES
  ('pizza', 'Italian', true, 1, 1.1),
  ('sushi', 'Japanese', true, 2, 1.3),
  ('burgers', 'American', false, 2, 2.2),
  ('crêpes', 'French', false, 3, 2.5),
  ('schnitzel', 'Austrian', false, 4, 3.4),
  ('tapas', 'Spanish', true, 4, 3.2),
  ('tacos', 'Mexican', false, 2, 2.1),
  ('chicken curry', 'Indian', true, 4, 3.3),
  ('bratwurst', 'German', false, 3, 3.6),
  ('fish and chips', 'British', false, 3, 4.3),
  ('couscous', 'Moroccan', true, 4, 3.2),
  ('pho', 'Vietnamese', true, 4, 4.4),
  ('egg noodles', 'Chinese', false, 5, 4.5);
