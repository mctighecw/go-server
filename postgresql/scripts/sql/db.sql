CREATE TABLE foods (
  id serial PRIMARY KEY,
  name varchar(40) NOT NULL,
  cuisine varchar(40) NOT NULL,
  healthy boolean NOT NULL,
  popularity int NOT NULL,
  rating float NOT NULL,
  created_at timestamptz DEFAULT NOW()
);
