#!/bin/bash

echo Setting up development database...

DB_USER="go_user"
DB_PW="mypassword"
DB_NAME="go_foods"

echo Creating user: $DB_USER. Please enter password: $DB_PW
createuser -U $USER -P $DB_USER

echo Creating new db...
createdb -U $USER -E 'utf8' -T 'template0' $DB_NAME

psql -U $DB_USER -d $DB_NAME -a -f ./sql/db.sql
psql -U $DB_USER -d $DB_NAME -a -f ./sql/seeds.sql
