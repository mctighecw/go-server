package server

import (
  "../handlers"
  "../lib"

  "net/http"
  "time"

  "github.com/gorilla/mux"
)

func CreateServer(port string) *http.Server {
  r := mux.NewRouter()

  // Custom logger
  r.Use(lib.Logger)

  // Routes
  r.HandleFunc("/", handlers.HelloWorld)
  r.HandleFunc("/hi/{name}", handlers.GreetUser)

  a := r.PathPrefix("/api").Subrouter()
  f := a.PathPrefix("/foods").Subrouter()
  f.HandleFunc("/all", handlers.GetAll)
  f.HandleFunc("/one", handlers.GetOne)
  f.HandleFunc("/search", handlers.SearchFilter)
  f.HandleFunc("/add", handlers.AddOne)
  f.HandleFunc("/update", handlers.UpdateOne)
  f.HandleFunc("/delete", handlers.DeleteOne)

  s := &http.Server{
    Handler: r,
    Addr: port,
    WriteTimeout: 15 * time.Second,
    ReadTimeout: 15 * time.Second,
  }

  // Info
  mode := lib.GetEnv("APP_ENV", "development")
  m := "Mode: " + mode
  lib.PrintMessage(m)

  return s
}
