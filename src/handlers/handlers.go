package handlers

import (
  "../db"
  "../lib"
  "../models"

  "database/sql"
  "fmt"
  "net/http"

  "github.com/gorilla/mux"
  "github.com/lib/pq"
)

func HelloWorld(w http.ResponseWriter, r *http.Request) {
  message := "Hello World!"
  w.Write([]byte(message))
}

func GreetUser(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  name := vars["name"]
  message := "Hi " + name
  w.Write([]byte(message))
}

func GetAll(w http.ResponseWriter, r *http.Request) {
  // GET - returns all foods (all rows and columns)
  valid := lib.CheckGetRequest(w, r)

  if valid {
    db := db.DbConnect()
    rows, err := db.Query("SELECT * FROM foods")
    lib.ErrorPanic(err)
    defer rows.Close()
    defer db.Close()

    m := lib.MapRows(rows)
    d := lib.ReturnAllData(m)

    w.Write([]byte(d))
  }
}

func GetOne(w http.ResponseWriter, r *http.Request) {
  // POST - returns one food by id
  valid := lib.CheckPostRequest(w, r)

  if valid {
    p, err := lib.DecodePostData(w, r)

    id := p.Id
    db := db.DbConnect()
    row := db.QueryRow("SELECT * FROM foods WHERE id = $1", id)

    var food models.Food
    err = row.Scan(&food.ID, &food.Name, &food.Cuisine,
      &food.Healthy, &food.Popularity, &food.Rating, &food.CreatedAt)
    defer db.Close()

    var res []byte

    if (err != nil) {
      if (err == sql.ErrNoRows) {
        res = lib.ReturnError("Not found")
      } else {
        res = lib.ReturnError("An error has occurred")
      }
    } else {
      res = lib.ReturnOneData(food)
    }

    w.Write([]byte(res))
  }
}

func SearchFilter(w http.ResponseWriter, r *http.Request) {
  // POST - returns a filtered list of matching results for a field
  valid := lib.CheckPostRequest(w, r)

  if valid {
    p, err := lib.DecodePostData(w, r)

    field := p.Field
    value := p.Value
    quotedField := pq.QuoteIdentifier(field)

    db := db.DbConnect()
    qStr1 := fmt.Sprintf("SELECT * FROM foods WHERE %s ", quotedField)
    qStr2 := qStr1 + "LIKE '%' || $1 || '%'"

    rows, err := db.Query(qStr2, value)
    lib.ErrorPanic(err)
    defer rows.Close()
    defer db.Close()

    m := lib.MapRows(rows)
    d := lib.ReturnAllData(m)

    w.Write([]byte(d))
  }
}

func AddOne(w http.ResponseWriter, r *http.Request) {
  // POST - adds a new food and returns id
  valid := lib.CheckPostRequest(w, r)

  if valid {
    f, err := lib.DecodeAddNewData(w, r)

    id := 0
    name := f.Name
    cuisine := f.Cuisine
    healthy := f.Healthy
    popularity := f.Popularity
    rating := f.Rating

    db := db.DbConnect()
    row := db.QueryRow(`
      INSERT INTO foods
      (name, cuisine, healthy, popularity, rating)
      VALUES ($1, $2, $3, $4, $5)
      RETURNING id`,
      name, cuisine, healthy, popularity, rating)

    err = row.Scan(&id)
    lib.ErrorPanic(err)
    defer db.Close()

    var res []byte

    if (err != nil) {
      res = lib.ReturnError("An error has occurred")
    } else {
      s := fmt.Sprintf("%d", id)
      m := "New food created with id " + s
      res = lib.ReturnOK(m)
    }

    w.Write([]byte(res))
  }
}

func UpdateOne(w http.ResponseWriter, r *http.Request) {
  // POST - updates one food by id, with field name and new value
  valid := lib.CheckPostRequest(w, r)

  if valid {
    p, err := lib.DecodePostData(w, r)

    id := p.Id
    field := p.Field
    value := p.Value
    quotedField := pq.QuoteIdentifier(field)

    db := db.DbConnect()
    res, err := db.Exec(fmt.Sprintf(`
      UPDATE foods
      SET %s = $1
      WHERE id = $2`,
      quotedField), value, id)
    lib.ErrorPanic(err)

    count, err := res.RowsAffected()
    lib.ErrorPanic(err)

    var resp []byte

    if (err != nil) {
      resp = lib.ReturnError("An error has occurred")
    } else if (count == 0) {
      resp = lib.ReturnError("Not found")
    } else {
      resp = lib.ReturnOK("Food has been updated")
    }

    w.Write([]byte(resp))
  }
}

func DeleteOne(w http.ResponseWriter, r *http.Request) {
  // POST - deletes one food by id
  valid := lib.CheckPostRequest(w, r)

  if valid {
    p, err := lib.DecodePostData(w, r)

    id := p.Id
    db := db.DbConnect()
    res, err := db.Exec("DELETE FROM foods WHERE id = $1", id)
    lib.ErrorPanic(err)

    count, err := res.RowsAffected()
    lib.ErrorPanic(err)

    var resp []byte

    if (err != nil) {
      resp = lib.ReturnError("An error has occurred")
    } else if (count == 0) {
      resp = lib.ReturnError("Not found")
    } else {
      resp = lib.ReturnOK("Food has been deleted")
    }

    w.Write([]byte(resp))
  }
}
