package db

import (
  "../conf"
  "../lib"

  "database/sql"
  "fmt"

  _ "github.com/lib/pq"
)

var (
  host = conf.DB_CONFIG.Host
  port = conf.DB_CONFIG.Port
  user = conf.DB_CONFIG.User
  password = conf.DB_CONFIG.Pw
  dbname = conf.DB_CONFIG.DbName
)

func DbConnect() *sql.DB {
  psqlInfo := fmt.Sprintf(`
    host=%s
    port=%d
    user=%s
    password=%s
    dbname=%s
    sslmode=disable`,
    host, port, user, password, dbname)

  db, err := sql.Open("postgres", psqlInfo)
  lib.ErrorPanic(err)

  err = db.Ping()
  lib.ErrorPanic(err)

  return db
}
