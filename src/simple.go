package main

import (
  "./conf"
  "./handlers"
  "./lib"

  "fmt"
  "net/http"
  "strings"
)

func greetUser(w http.ResponseWriter, r *http.Request) {
  m := r.URL.Path
  m = strings.TrimPrefix(m, "/hi/")
  m = "Hi " + m
  w.Write([]byte(m))
}

func main() {
  p := conf.APP_CONFIG.Port
  ps := fmt.Sprintf(":%d", p)

  // Routes
  http.HandleFunc("/", handlers.HelloWorld)
  http.HandleFunc("/hi/", greetUser)

  // Server info
  i := fmt.Sprintf("Starting Go server on port %d", p)
  lib.PrintMessage(i)

  if err := http.ListenAndServe(ps, nil); err != nil {
    panic(err)
  }
}
