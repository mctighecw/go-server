package lib

import (
  "database/sql"
  "encoding/json"
  "fmt"
  "os"
)

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorPanic(err error) {
  if err != nil {
    panic(err)
  }
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func ReturnOK(m string) []byte {
  r := map[string]interface{}{"status": "OK", "message": m}
  data, err := json.Marshal(r)
  LogError(err)

  return data
}

func ReturnError(m string) []byte {
  r := map[string]interface{}{"status": "Error", "message": m}
  data, err := json.Marshal(r)
  LogError(err)

  return data
}

func ReturnAllData(d []map[string]interface {}) []byte {
  r := map[string]interface{}{"status": "OK", "data": d}
  data, err := json.Marshal(r)
  LogError(err)

  return data
}

func ReturnOneData(d interface {}) []byte {
  r := map[string]interface{}{"status": "OK", "data": d}
  data, err := json.Marshal(r)
  LogError(err)

  return data
}

func MapRows(rows *sql.Rows) []map[string]interface {} {
  // See: https://stackoverflow.com/questions/19991541/dumping-mysql-tables-to-json-with-golang
  columns, err := rows.Columns()
  LogError(err)

  count := len(columns)
  tableData := make([]map[string]interface{}, 0)
  values := make([]interface{}, count)
  valuePtrs := make([]interface{}, count)

  for rows.Next() {
    for i := 0; i < count; i++ {
      valuePtrs[i] = &values[i]
    }

    rows.Scan(valuePtrs...)
    entry := make(map[string]interface{})

    for i, col := range columns {
      var v interface{}
      val := values[i]
      b, ok := val.([]byte)

      if ok {
        v = string(b)
      } else {
        v = val
      }
      entry[col] = v
    }
    tableData = append(tableData, entry)
  }

  return tableData
}
