package lib

import (
  "../models"

  "encoding/json"
  "log"
  "net/http"
  "time"
)

type PostData struct {
  Id int `json:"id"`
  Field string `json:"field"`
  Value string `json:"value"`
}

func Logger(next http.Handler) http.Handler {
  // See: https://golangcode.com/attach-a-logger-to-your-router/
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    start := time.Now()

    log.Printf(
      "\t%s\t%s\t\t%s\t",
      r.Method,
      r.RequestURI,
      time.Since(start),
    )
    next.ServeHTTP(w, r)
  })
}

func CheckGetRequest(w http.ResponseWriter, r *http.Request) bool {
  if r.Method != "GET" {
    http.Error(w, http.StatusText(405), 405)
    return false
  }
  return true
}

func CheckPostRequest(w http.ResponseWriter, r *http.Request) bool {
  if r.Method != "POST" {
    http.Error(w, http.StatusText(405), 405)
    return false
  }

  if r.Body == nil {
    http.Error(w, "Bad request", 400)
    return false
  }
  return true
}

func DecodePostData(w http.ResponseWriter, r *http.Request) (PostData, error) {
  var p PostData
  err := json.NewDecoder(r.Body).Decode(&p)

  if err != nil {
    http.Error(w, err.Error(), 400)
  }
  return p, err
}

func DecodeAddNewData(w http.ResponseWriter, r *http.Request) (models.Food, error) {
  var f models.Food
  err := json.NewDecoder(r.Body).Decode(&f)

  if err != nil {
    http.Error(w, err.Error(), 400)
  }
  return f, err
}
