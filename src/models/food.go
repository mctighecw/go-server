package models

import (
  "time"
)

type Food struct {
  ID int `json:"id"`
  Name string `json:"name"`
  Cuisine string `json:"cuisine"`
  Healthy bool `json:"healthy"`
  Popularity int `json:"popularity"`
  Rating float64 `json:"rating"`
  CreatedAt time.Time `json:"created_at"`
}
