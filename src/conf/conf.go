package conf

import (
  "os"
)

type (
  App struct {
    Name string
    Port uint
  }

  Db struct {
    Host string
    Port uint
    User string
    Pw string
    DbName string
  }
)

var (
  APP_CONFIG *App
  DB_CONFIG *Db

  APP_ENV string
  DB_HOST string
  DB_USER string
  DB_PW string
  DB_NAME string
)

func init() {
  APP_CONFIG = &App {
    Name: "Go Server",
    Port: 9500,
  }

  APP_ENV = os.Getenv("APP_ENV")

  if APP_ENV == "production" {
    DB_HOST = os.Getenv("DB_HOST")
    DB_USER = os.Getenv("DB_USER")
    DB_PW = os.Getenv("DB_PW")
    DB_NAME = os.Getenv("DB_NAME")
  } else {
    DB_HOST = "localhost"
    DB_USER = "go_user"
    DB_PW = "mypassword"
    DB_NAME = "go_foods"
  }

  DB_CONFIG = &Db {
    Host: DB_HOST,
    Port: 5432,
    User: DB_USER,
    Pw: DB_PW,
    DbName: DB_NAME,
  }
}
