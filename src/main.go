package main

import (
  "./conf"
  "./lib"
  "./server"

  "fmt"
  "log"
)

func main() {
  p := conf.APP_CONFIG.Port
  ps := fmt.Sprintf(":%d", p)
  s := server.CreateServer(ps)

  // Server info
  i := fmt.Sprintf("Starting Go server on port %d", p)
  lib.PrintMessage(i)

  log.Fatal(s.ListenAndServe())
}
