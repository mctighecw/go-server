FROM golang:1.13

WORKDIR /usr/src

RUN go get -u "github.com/gorilla/mux"
RUN go get -u "github.com/lib/pq"

COPY . .

EXPOSE 9500

CMD ["go", "run", "./src/main.go"]
