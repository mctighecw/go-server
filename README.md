# README

My second project using Go (_Golang_). It is a basic web server using the Go package _net/http_, without any web framework. The Gorilla toolkit _mux_ package is also used for routing. It is connected to a PostgreSQL database.

There is a basic REST API to add, get, update, search (filter) and delete items in the _go_foods_ database.

## App Information

App Name: go-server

Created: November 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/go-server)

## Tech Stack

- Go
- [Gorilla mux](https://www.gorillatoolkit.org/pkg/mux)
- [pq](https://godoc.org/github.com/lib/pq) Postgres Driver
- PostgreSQL
- Docker

## To Run

### Setup & Run via Terminal

1. Install Go locally
2. Set `$GOPATH`
3. Install dependencies:

```
$ cd go-server
$ go get -u "github.com/gorilla/mux"
$ go get -u "github.com/lib/pq"
```

3. Initialize database

```
$ cd go-server/postgresql/scripts
$ source create_db_dev.sh
```

4. Drop database, if necessary (later on)

```
$ cd go-server/postgresql/scripts
$ source drop_db.sh
```

5. Start server

```
$ cd go-server
$ source run_dev.sh
```

Server running at `http://localhost:9500/`

### With Docker

Add .env file with necessary variables to project root

```
$ docker-compose up -d --build
$ source init_docker_db.sh
```

## Routes

- Get all items:

```
GET
URL: http://localhost:9500/api/foods/all
```

- Get one item:

```
POST
DATA: { "id": 3 }
URL: http://localhost:9500/api/foods/one
```

- Search (filter) one or more:

```
POST
DATA: { "field": "name", "value": "ta" }
URL: http://localhost:9500/api/foods/search
```

- Add new item:

```
POST
DATA: {
  "name": "goulash",
  "cuisine": "Hungarian",
  "healthy": false,
  "popularity": 4,
  "rating": 3.6
}
URL: http://localhost:9500/api/foods/add
```

- Update existing item:

```
POST
DATA: {
  "id": 1,
  "field": "name",
  "value": "pizzas"
}
URL: http://localhost:9500/api/foods/update
```

- Delete item:

```
POST
DATA: { "id": 5 }
URL: http://localhost:9500/api/foods/delete
```

Last updated: 2025-02-11
